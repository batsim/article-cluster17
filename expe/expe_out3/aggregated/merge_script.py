#!/usr/bin/env python3
import glob
import re
import shutil
import os
import pandas as pd
import subprocess

def replace_sleep_constraint(x):
    if x['ensured_sleep_bounds__name'] == 'zero':
        return 0
    elif x['ensured_sleep_bounds__name'] == 'T':
        return 't_safe'
    else:
        return x['ensured_sleep_bounds__name']

def replace_algorithm_name(x):
    if x['algo__name'] == 'easy_bf':
        return 'EASY'
    elif x['algo__name'] == 'inertial_shutdown':
        res = 'inertial'
        if x['needed_time_to_sedate'] < 6000:
            res = res + 'OS'
        return res
    elif x['algo__name'] == 'subpart_sleeper':
        res = 'proportional'
        if x['needed_time_to_sedate'] < 6000:
            res = res + 'OS'
        return res
    else:
        return x['algo__name']

########################################
# Let's merge the schedules' overviews #
########################################

instances_info_df = pd.read_csv('expe_out3/instances/instances_info.csv')

filenames = glob.glob('expe_out3/aggregated/links/*_schedule.csv')
df_list = []
for filename in filenames:
    m = re.search('expe_out3/aggregated/links/(.*)_schedule\.csv', filename)
    instance_id = m.group(1)

    schedule_df = pd.read_csv(filename)
    jobs_filename = 'expe_out3/aggregated/links/{i}_jobs.csv'.format(i=instance_id)
    #jobs_filenames.append(jobs_filename)
    jobs_df = pd.read_csv(jobs_filename)

    pstates_fname = 'expe_out3/aggregated/links/{i}_pstate_changes.csv'.format(i=instance_id)
    nb_pstates_p = subprocess.run('cat {} | wc -l'.format(pstates_fname),
                                  stdout=subprocess.PIPE, shell=True)
    nb_pstates = int(nb_pstates_p.stdout.strip()) // 2

    schedule_df['instance_id'] = instance_id
    schedule_df['mean_waiting_time'] = jobs_df['waiting_time'].mean()
    schedule_df['max_waiting_time'] = jobs_df['waiting_time'].max()
    schedule_df['mean_slowdown'] = jobs_df['stretch'].mean()
    schedule_df['max_slowdown'] = jobs_df['stretch'].max()
    schedule_df['nb_pstate_switches'] = nb_pstates

    df_list.append(schedule_df)

aggregated_df = pd.concat(df_list, ignore_index=True)
joined_df = pd.merge(aggregated_df, instances_info_df, on='instance_id')

# Let's rename some data
joined_df["ensured_sleep_bounds__name"] = joined_df.apply(replace_sleep_constraint, axis=1)
joined_df["algo__name"] = joined_df.apply(replace_algorithm_name, axis=1)

# Let's rename some algorithms
#joined_df.ix[joined_df.algo__name == 'energy_bf_idle_sleeper', 'algo__name'] = 'ebf_idle'
#joined_df.ix[joined_df.algo__name == 'energy_bf_subpart_sleeper', 'algo__name'] = 'ebf_subpart'
#joined_df.ix[joined_df.algo__name == 'easy_bf_plot', 'algo__name'] = 'easy'

joined_df.to_csv('expe_out3/aggregated/schedules_aggregated.csv',
                 index=False, na_rep = 'NA')
