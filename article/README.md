This directory contains files to generate the article.

# Dependencies
- A latex compiler
- rubber (latex build system)
- asymptote (script-based vector graphics language compiler)

# Generate the article
``` bash
make
```
