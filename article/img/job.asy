settings.outformat = "pdf";
unitsize(1cm);
import patterns;
//defaultpen(fontsize(20));
//defaultpen(linewidth(1));

real point_size = 0.2;

struct Job
{
    string id;
    int submission_date;
    int processing_time;
    int requested_time;
    int number_of_requested_resources;
    real starting_time;
    real allocation_start;
    pen fill_color;
}

add("tile",tile(4mm, dotted));
add("hatch",hatch(4mm, linetype(new real[] {0,8})));
add("hatchback",hatch(NW));
add("brick",brick());
add("crosshatch",crosshatch());

void draw_job(Job j, bool draw_requested_time = false, bool draw_id = true,
              string requested_time_pattern = "hatch",
              bool erase_requested_time_split = true)
{
    path executed_path = (j.starting_time, j.allocation_start) --
                         (j.starting_time + j.processing_time, j.allocation_start) --
                         (j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources) --
                         (j.starting_time, j.allocation_start + j.number_of_requested_resources) -- cycle;

    path non_executed_path = (j.starting_time + j.processing_time, j.allocation_start) --
                             (j.starting_time + j.requested_time, j.allocation_start) --
                             (j.starting_time + j.requested_time, j.allocation_start + j.number_of_requested_resources) --
                             (j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources) --
                             cycle;

    if (draw_requested_time && j.requested_time > j.processing_time)
    {
        filldraw((j.starting_time, j.allocation_start) --
                 (j.starting_time + j.requested_time, j.allocation_start) --
                 (j.starting_time + j.requested_time, j.allocation_start + j.number_of_requested_resources) --
                 (j.starting_time, j.allocation_start + j.number_of_requested_resources) -- cycle,
                 j.fill_color, invisible);
    }
    else
        filldraw(executed_path, j.fill_color, invisible);

    // Left borders
    draw((j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources) --
         (j.starting_time, j.allocation_start + j.number_of_requested_resources) --
         (j.starting_time, j.allocation_start) --
         (j.starting_time + j.processing_time, j.allocation_start));

    if (draw_id)
    {
        pair id_loc = (j.starting_time, j.allocation_start) +
                      (j.processing_time, j.number_of_requested_resources) / 2;
        label((string)j.id, id_loc);
    }

    if (draw_requested_time && j.requested_time > j.processing_time)
    {
        filldraw(non_executed_path, pattern(requested_time_pattern), invisible);

        draw((j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources) --
             (j.starting_time + j.requested_time, j.allocation_start + j.number_of_requested_resources) --
             (j.starting_time + j.requested_time, j.allocation_start) --
             (j.starting_time + j.processing_time, j.allocation_start));

        // Middle vertical border (dotted)
        draw((j.starting_time + j.processing_time, j.allocation_start) --
             (j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources),
             linetype(new real[] {1,8}));
    }
    else
    {
        // Middle vertical border
        draw((j.starting_time + j.processing_time, j.allocation_start) --
             (j.starting_time + j.processing_time, j.allocation_start + j.number_of_requested_resources));
    }
}

void draw_job_on_position(Job j, pair position,
                          bool draw_requested_time = false,
                          bool draw_id = true,
                          string requested_time_pattern = "hatch",
                          bool erase_requested_time_split = true)
{
    Job j2 = j;
    j.allocation_start = position.y;
    j.starting_time = position.x;

    draw_job(j2, draw_requested_time=draw_requested_time,
             draw_id=draw_id,
             requested_time_pattern=requested_time_pattern,
             erase_requested_time_split);
}

void draw_frame(real t0, real t1, real m0, real m1, real dash_width=1/8,
                bool draw_time_axe_values = true,
                bool draw_time_axe_label = true,
                bool draw_machine_axe_values = true,
                bool draw_machines_separations = true,
                bool draw_time_axe = true,
                bool draw_machines_axe = true)
{
    // Time axe
    if (draw_time_axe)
    {
        draw((t0,m0) -- (t1+2,m0), Arrow);
        if (draw_time_axe_label)
            label("$time$", (t1+2,m0), align=right);

        // Time ticks
        for (int i = (int)t0; i <= (int)t1; ++i)
        {
            draw((i,m0-dash_width) -- (i,m0), black);
            if (draw_time_axe_values)
                label((string)i, (i,m0-dash_width), align=down);
        }
    }

    // Machine axe
    if (draw_machines_axe)
    {
        draw((t0,m0) -- (t0, m1));

        // Machine ticks
        draw((t0-dash_width,m0) -- (t0,m0), black);

        for (int i = (int)m0+1; i <= (int)m1; ++i)
        {
            draw((t0-dash_width,i) -- (t0,i), black);
            if (draw_machine_axe_values)
                label("M"+(string)i, (t0-dash_width,i-0.5), align=left);
        }
    }

    // Machine lines
    if (draw_machines_separations)
    {
        for (int i = (int)m0+1; i <= (int)m1; ++i)
            draw((t0,i) -- (t1,i), black+linewidth(0.2)+linetype(new real[] {16,8,0,32}));
    }
}

void figure_one_job_definition()
{
    // The job
    Job j;
    j.id = "$j$";
    j.submission_date = 0;
    j.processing_time = 3;
    j.requested_time = 4;
    j.number_of_requested_resources = 2;
    j.starting_time = 1;
    j.allocation_start = 0;
    j.fill_color = rgb(251,180,174);

    draw_job(j, true);

    // The processing time
    real line_y = j.allocation_start - 0.25;
    draw((j.starting_time, line_y) --
         (j.starting_time + j.processing_time, line_y),
         black, Arrow);
    draw((j.starting_time, line_y) --
         (j.starting_time + j.processing_time, line_y) -- cycle,
         black, Arrow);
    label("$p_j$", (j.starting_time + j.processing_time/2.0, line_y), align=down);

    // The requested time
    real line_y = j.allocation_start + j.number_of_requested_resources + 0.5;
    draw((j.starting_time, line_y) --
         (j.starting_time + j.requested_time, line_y),
         black, Arrow);
    draw((j.starting_time, line_y) --
         (j.starting_time + j.requested_time, line_y) -- cycle,
         black, Arrow);
    label("$wall_j$", (j.starting_time + j.requested_time/2.0, line_y+0.1), align=down);

    // The number of requested resources
    real line_x = j.starting_time - 0.25;
    draw((line_x, j.allocation_start) --
         (line_x, j.allocation_start + j.number_of_requested_resources),
         Arrow);
    draw((line_x, j.allocation_start) --
         (line_x, j.allocation_start + j.number_of_requested_resources) -- cycle,
         Arrow);
    label("$q_j$", (line_x, j.allocation_start + j.number_of_requested_resources/2.0),
          align=left);

    shipout("one_job");
}

void figure_one_job_more_attributes()
{
    // The job
    Job j;
    j.id = "$j$";
    j.submission_date = 1;
    j.processing_time = 3;
    j.requested_time = 4;
    j.number_of_requested_resources = 2;
    j.starting_time = 3;
    j.allocation_start = 1;
    j.fill_color = rgb(128,32,32);

    draw_frame(0, 5, 0, 3, draw_time_axe_values=false,
               draw_machine_axe_values=false,
               draw_machines_separations=false, draw_machines_axe = false);
    draw_job(j);

    // Release date bar
    real bars_y = 4;
    real times_y = -1/8;
    draw((j.submission_date,0) -- (j.submission_date,bars_y), black+linetype(new real[] {8,4}));
    label("$r_j$", (j.submission_date,times_y), align=down);

    // Starting date bar
    draw((j.starting_time,0) -- (j.starting_time,bars_y), black+linetype(new real[] {8,4}));
    label("$start_j$", (j.starting_time,times_y), align=down);

    // Completion date bar
    draw((j.starting_time + j.processing_time,0) -- (j.starting_time + j.processing_time,bars_y), black+linetype(new real[] {8,4}));
    label("$C_j$", (j.starting_time + j.processing_time,times_y), align=down);

    // Waiting time display
    real line_y = 3.25;
    draw((j.submission_date, line_y) -- (j.starting_time, line_y), Arrow);
    draw((j.submission_date, line_y) -- (j.starting_time, line_y) -- cycle, Arrow);
    label("$wait_j$", ((j.submission_date+j.starting_time)/2.0, line_y), align=up);

    // Turnaround time display
    line_y = 4;
    draw((j.submission_date, line_y) -- (j.starting_time+j.processing_time, line_y), Arrow);
    draw((j.submission_date, line_y) -- (j.starting_time+j.processing_time, line_y) -- cycle, Arrow);
    label("$turnaround_j$", ((j.submission_date+j.starting_time+j.processing_time)/2.0, line_y), align=up);

    shipout("one_job_context");
}

Job[] generate_workload()
{
    Job j1, j2, j3, j4;

    j1.id = "$1$";
    j1.submission_date = 0;
    j1.processing_time = 2;
    j1.requested_time = 3;
    j1.number_of_requested_resources = 2;
    j1.fill_color = rgb(251,180,174);

    j2.id = "$2$";
    j2.submission_date = 0;
    j2.processing_time = 1;
    j2.requested_time = 2;
    j2.number_of_requested_resources = 2;
    j2.fill_color = rgb(179,205,227);

    j3.id = "$3$";
    j3.submission_date = 1;
    j3.processing_time = 3;
    j3.requested_time = 5;
    j3.number_of_requested_resources = 2;
    j3.fill_color = rgb(204,235,197);

    j4.id = "$4$";
    j4.submission_date = 3;
    j4.processing_time = 1;
    j4.requested_time = 2;
    j4.number_of_requested_resources = 4;
    j4.fill_color = rgb(222,203,228);

    Job jobs[] = {j1, j2, j3, j4};

    return jobs;
}

Job[] generate_energy_jobs()
{
    Job json, jsoff, joff;

    json.id = "on";
    json.processing_time = 2;
    json.number_of_requested_resources = 1;
    json.fill_color = rgb("56ae6c");

    jsoff.id = "off";
    jsoff.processing_time = 1;
    jsoff.number_of_requested_resources = 1;
    jsoff.fill_color = rgb("ba495b");

    joff.id = "";
    joff.processing_time = 2;
    joff.number_of_requested_resources = 1;
    joff.fill_color = rgb("444444");

    Job[] jobs = {json, jsoff, joff};
    return jobs;
}

void figure_workload_example()
{
    // Let's define the jobs
    Job jobs[] = generate_workload();
    Job energy_jobs[] = generate_energy_jobs();
    Job j1, j2, j3, j4, json, jsoff, joff;

    j1 = jobs[0];
    j2 = jobs[1];
    j3 = jobs[2];
    j4 = jobs[3];
    json = energy_jobs[0];
    jsoff = energy_jobs[1];
    joff = energy_jobs[2];

    joff.processing_time = 4;

    // Let's draw the jobs
    real base_y = 0;

    draw_job_on_position(j1, (0,base_y+2.5), draw_requested_time=true);
    draw_job_on_position(j2, (3.5,base_y+2.5), draw_requested_time=true);
    draw_job_on_position(j3, (0,base_y+0), draw_requested_time=true);
    draw_job_on_position(j4, (6,base_y+0.5), draw_requested_time=true);
    draw_job_on_position(json, (9,base_y+3.5));
    draw_job_on_position(jsoff, (9.5,base_y+1.5));
    draw_job_on_position(joff, (6.5, base_y-0.75));

    // Let's draw the submission times
    draw_frame(0,3, base_y-1.5,0,
               draw_machines_axe = false,
               draw_machines_separations = false);

    for (int i = 0; i < jobs.length; ++i)
    {
        draw((jobs[i].submission_date, base_y-1) --
             (jobs[i].submission_date, base_y-1.5), Arrow);
    }

    label("$r_1$", (j1.submission_date, base_y-1), align=up);
    label("$r_2$", (j2.submission_date, base_y-0.6), align=up);
    label("$r_3$", (j3.submission_date, base_y-1), align=up);
    label("$r_4$", (j4.submission_date, base_y-1), align=up);

    // Let's draw q_j
    draw((0-0.25, base_y+2.5) -- (0-0.25, base_y+2.5+j1.number_of_requested_resources), Arrow);
    draw((0-0.25, base_y+2.5) -- (0-0.25, base_y+2.5+j1.number_of_requested_resources) -- cycle, Arrow);
    label((string)j1.number_of_requested_resources, (0-0.25, base_y+2.5+(j1.number_of_requested_resources/2.0)), align=left);

    draw((0-0.25, base_y+0) -- (0-0.25, base_y+0+j3.number_of_requested_resources), Arrow);
    draw((0-0.25, base_y+0) -- (0-0.25, base_y+0+j3.number_of_requested_resources) -- cycle, Arrow);
    label((string)j3.number_of_requested_resources, (0-0.25, base_y+0+(j3.number_of_requested_resources/2.0)), align=left);

    draw((6+j4.requested_time+0.25, base_y+0.5) -- (6+j4.requested_time+0.25, base_y+0.5+j4.number_of_requested_resources), Arrow);
    draw((6+j4.requested_time+0.25, base_y+0.5) -- (6+j4.requested_time+0.25, base_y+0.5+j4.number_of_requested_resources) -- cycle, Arrow);
    label((string)j4.number_of_requested_resources, (6+j4.requested_time+0.25, base_y+0.5+(j4.number_of_requested_resources/2.0)), align=right);

    // Let's draw p_j of switch jobs
    draw((9,base_y+3.5-0.25) -- (9+json.processing_time,base_y+3.5-0.25), black, Arrow);
    draw((9,base_y+3.5-0.25) -- (9+json.processing_time,base_y+3.5-0.25) -- cycle, black, Arrow);
    label((string)json.processing_time, (9+(json.processing_time/2.0),base_y+3.5-0.25), align=down);

    draw((9.5, base_y+1.5-0.25) -- (9.5+jsoff.processing_time,base_y+1.5-0.25), black, Arrow);
    draw((9.5, base_y+1.5-0.25) -- (9.5+jsoff.processing_time,base_y+1.5-0.25) -- cycle, black, Arrow);
    label((string)jsoff.processing_time, (9.5+(jsoff.processing_time/2.0),base_y+1.5-0.25), align=down);

    draw((6.5, base_y-0.75-0.25) -- (6.5+joff.processing_time,base_y-0.75-0.25), black, Arrow);
    draw((6.5, base_y-0.75-0.25) -- (6.5+joff.processing_time,base_y-0.75-0.25) -- cycle, black, Arrow);
    label("$\ge0$", (6.5+(joff.processing_time/2.0),base_y-0.75-0.25), align=down);

    shipout("workload_example");
}

void figure_gantt_easy()
{
    // Let's define the jobs
    Job jobs[] = generate_workload();
    Job j1, j2, j3, j4;

    j1 = jobs[0];
    j2 = jobs[1];
    j3 = jobs[2];
    j4 = jobs[3];

    // Let's draw the frame
    draw_frame(0,6, 0,4);

    // Let's draw the jobs
    draw_job_on_position(j1, (0,0));
    draw_job_on_position(j2, (0,2));
    draw_job_on_position(j3, (1,2));
    draw_job_on_position(j4, (4,0));

    shipout("gantt_easy");
}

void figure_gantt_switch_off()
{
    // Let's define the jobs
    Job jobs[] = generate_workload();
    Job energy_jobs[] = generate_energy_jobs();
    Job j1, j2, j3, j4, json, jsoff, joff;

    j1 = jobs[0];
    j2 = jobs[1];
    j3 = jobs[2];
    j4 = jobs[3];
    json = energy_jobs[0];
    jsoff = energy_jobs[1];
    joff = energy_jobs[2];

    // Let's draw the frame
    draw_frame(0,7, 0,4);

    // Let's draw the jobs
    draw_job_on_position(j1, (0,0));
    draw_job_on_position(j2, (0,2));
    draw_job_on_position(j3, (1,2));
    draw_job_on_position(j4, (5,0));
    draw_job_on_position(jsoff, (2,0));
    draw_job_on_position(jsoff, (2,1));
    draw_job_on_position(json, (3,0));
    draw_job_on_position(json, (3,1));

    shipout("gantt_switch_off");
}

void figure_gantt_offline()
{
    // Let's define the jobs
    Job jobs[] = generate_workload();
    Job energy_jobs[] = generate_energy_jobs();
    Job j1, j2, j3, j4, json, jsoff, joff;

    j1 = jobs[0];
    j2 = jobs[1];
    j3 = jobs[2];
    j4 = jobs[3];
    json = energy_jobs[0];
    jsoff = energy_jobs[1];
    joff = energy_jobs[2];

    // Let's draw the frame
    draw_frame(0,8, 0,4);

    // Let's draw the jobs
    draw_job_on_position(jsoff, (0,0));
    draw_job_on_position(jsoff, (0,1));
    draw_job_on_position(jsoff, (0,2));
    draw_job_on_position(jsoff, (0,3));

    draw_job_on_position(joff, (1,0));
    draw_job_on_position(joff, (1,1));
    draw_job_on_position(joff, (1,2));
    draw_job_on_position(joff, (1,3));

    draw_job_on_position(json, (3,0));
    draw_job_on_position(json, (3,1));
    draw_job_on_position(json, (3,2));
    draw_job_on_position(json, (3,3));

    draw_job_on_position(j1, (5,0));
    draw_job_on_position(j2, (7,0));
    draw_job_on_position(j3, (5,2));
    draw_job_on_position(j4, (8,0));

    shipout("gantt_offline");
}

Job[] generate_llh_jobs()
{
    Job j1, j2, j3, j4;

    j1.id = "1";
    j1.number_of_requested_resources = 1;
    j1.processing_time = 2;
    j1.requested_time = 3;

    j2.id = "2";
    j2.number_of_requested_resources = 3;
    j2.processing_time = 1;
    j2.requested_time = 2;

    j3.id = "3";
    j3.number_of_requested_resources = 2;
    j3.processing_time = 2;
    j3.requested_time = 3;

    j4.id = "4";
    j4.number_of_requested_resources = 1;
    j4.processing_time = 1;
    j4.requested_time = 1;

    j1.fill_color = rgb(251,180,174);
    j2.fill_color = rgb(179,205,227);
    j3.fill_color = rgb(204,235,197);
    j4.fill_color = rgb(222,203,228);

    Job[] jobs = {j1, j2, j3, j4};
    return jobs;
}

void figures_llh()
{
    // Let's define the jobs
    Job jobs[] = generate_llh_jobs();
    Job energy_jobs[] = generate_energy_jobs();
    Job j1, j2, j3, j4, json, jsoff, joff;

    j1 = jobs[0];
    j2 = jobs[1];
    j3 = jobs[2];
    j4 = jobs[3];
    json = energy_jobs[0];
    jsoff = energy_jobs[1];
    joff = energy_jobs[2];

    joff.processing_time = 3;

    // Figure 1: previsional schedule
    // Let's draw the frame
    draw_frame(0,3, 0,5, draw_time_axe_values=false);
    real dash_width = 1/8;
    label("$t$", (0,0-dash_width), align=down);
    label("$t\!+\!1$", (1,0-dash_width), align=down);
    label("$t\!+\!2$", (2,0-dash_width), align=down);
    label("$t\!+\!3$", (3,0-dash_width), align=down);

    // Let's draw the jobs
    draw_job_on_position(j1, (0,0), draw_requested_time=true);
    draw_job_on_position(j2, (0,1), draw_requested_time=true);
    draw_job_on_position(joff, (0,4), draw_requested_time=false);

    // Draw infinite OFF
    draw((3,4) -- (4,4), dotted);
    draw((3,5) -- (4,5), dotted);
    label("$\rightarrow \infty$", (3.5, 4.5));

    label("Queue (load $l = 7$)", (6.5,4.5));
    draw_job_on_position(j3, (5,2), draw_requested_time=true);
    draw_job_on_position(j4, (5,1), draw_requested_time=true);

    // Invisible point to make alignment easier
    draw((0,6) -- cycle, invisible);
    shipout("llh_previsional_schedule");
    erase();

    // Figure 2: llh
    // Let's draw the frame
    draw_frame(0,4, 0,5, draw_time_axe_values=false);
    real dash_width = 1/8;
    label("$t$", (0,0-dash_width), align=down);
    label("$t\!+\!1$", (1,0-dash_width), align=down);
    label("$t\!+\!2$", (2,0-dash_width), align=down);
    label("$t\!+\!3$", (3,0-dash_width), align=down);
    label("$t\!+\!4$", (4,0-dash_width), align=down);

    // Let's draw the jobs
    draw_job_on_position(j1, (0,0), draw_requested_time=true);
    draw_job_on_position(j2, (0,1), draw_requested_time=true);
    joff.processing_time=5;
    draw_job_on_position(joff, (0,4), draw_requested_time=false);

    // Draw infinite OFF
    draw((5,4) -- (6,4), dotted);
    draw((5,5) -- (6,5), dotted);
    label("$\rightarrow \infty$", (5.5, 4.5));

    // Let's draw the LLH area
    draw_job_on_position(j4, (2,1), draw_id=false);

    j3.requested_time=2;
    draw_job_on_position(j3, (2,2), draw_id=false);

    j3.requested_time=1;
    j3.processing_time=0;
    draw_job_on_position(j3, (3,0), draw_id=false, draw_requested_time=true,
                         erase_requested_time_split = false);

    // Let's draw the LLH
    draw((0,5.25) -- (4,5.25), Arrow);
    draw((0,5.25) -- (4,5.25) -- cycle, Arrow);
    label("$LLH=4$", (2,5.25), align=up);

    // Invisible point to make alignment easier
    draw((0,6) -- cycle, invisible);
    shipout("llh_visualised");
}

void figures_sleep_lb()
{
    Job energy_jobs[] = generate_energy_jobs();
    Job json, jsoff, joff;
    json = energy_jobs[0];
    jsoff = energy_jobs[1];
    joff = energy_jobs[2];

    json.processing_time = 1;

    draw_frame(0,5, 0,5);

    for (int machine = 1; machine < 5; ++machine)
    {
        draw_job_on_position(jsoff, (0,machine));

        joff.processing_time = machine - 1;
        draw_job_on_position(joff, (1, machine));

        draw_job_on_position(json, (machine, machine));
    }

    shipout("sleep_lb_schedule");
    erase();

    // Let's compute the energy values of the machines
    real scale = 0.5;
    real p_idle = 2 * scale;
    real p_off = 1 * scale;
    real p_soff = 3 * scale;
    real p_son = 3 * scale;

    real[][] energy = {{0,0,0,0,0,0},
                       {0,0,0,0,0,0},
                       {0,0,0,0,0,0},
                       {0,0,0,0,0,0},
                       {0,0,0,0,0,0}};

    // Machine 1
    for (int time = 1; time <= 5; ++time)
        energy[0][time] = energy[0][time-1] + p_idle;

    // Switch OFF
    for (int machine = 1; machine < 5; ++machine)
        energy[machine][1] = energy[machine][0] + p_son;

    // Machine 2
    energy[1][2] = energy[1][1] + p_son;
    for (int time = 3; time <= 5; ++time)
        energy[1][time] = energy[1][time-1] + p_idle;

    // Machine 3
    energy[2][2] = energy[2][1] + p_off;
    energy[2][3] = energy[2][2] + p_son;
    for (int time = 4; time <= 5; ++time)
        energy[2][time] = energy[2][time-1] + p_idle;

    // Machine 4
    energy[3][2] = energy[3][1] + p_off;
    energy[3][3] = energy[3][2] + p_off;
    energy[3][4] = energy[3][3] + p_son;
    for (int time = 5; time <= 5; ++time)
        energy[3][time] = energy[3][time-1] + p_idle;

    // Machine 5
    energy[4][2] = energy[4][1] + p_off;
    energy[4][3] = energy[4][2] + p_off;
    energy[4][4] = energy[4][3] + p_off;
    energy[4][5] = energy[4][4] + p_son;

    // Frame
    real ymax = 0;
    for (int machine = 0; machine < 4; ++machine)
        ymax = max(ymax, energy[machine][5]);

    draw_frame(0,5, 0,5, draw_machine_axe_values=false,
               draw_machines_axe = false,
               draw_machines_separations = false);

    draw((0,0) -- (0,ymax), Arrow);
    label("$energy$", (0,ymax), align=up);

    pen[] pens={black,
                rgb(217,95,2)   + dashed,
                rgb(117,112,179) + longdashed,
                rgb(231,41,138) + dashdotted,
                rgb(31,120,180) + longdashdotted
    };

    // Let's draw the energy of machines
    for (int machine = 0; machine < 5; ++machine)
    {
        for (int time = 1; time <= 5; ++time)
        {
            draw((time-1, energy[machine][time-1]) -- (time, energy[machine][time]),
                 pens[machine]);
        }
        if (machine != 0 && machine != 3)
            label("M"+(string)(machine+1), (5, energy[machine][5]), align=right);
    }

    label("M1 = M4", (5, energy[0][5]), align=right);

    shipout("sleep_lb_energy");
}

// Let's draw the figures.
figure_one_job_definition();
erase();

// figure_one_job_more_attributes();
// erase();

// figure_workload_example();
// erase();

// figure_gantt_easy();
// erase();

//figure_gantt_switch_off();
//erase();

// figure_gantt_offline();
// erase();

figures_llh();
erase();

figures_sleep_lb();
erase();
